import { funcionDeEjemplo } from './index'

describe('Descripcion del objeto de prueba', () => {
  test('Descripcion de la prueba. ejemplo: funcion de prueba debe retornar 0', () => {
    const ejemplo = funcionDeEjemplo()
    expect(ejemplo).toBe(0)
  })
})

// Cuando esté listo para probar todo, descomentar el siguiente test
// ------------------------------------------------------------------
// import { obtenerNumeroRomano } from './index'
// import romanos1000 from './romanos1000.json'
// const limite = 10 // limitar casos de prueba, por velocidad
// const romanos: [number, string][] = romanos1000
//   .filter(({ decimal }) => decimal <= limite)
//   .map(({ decimal, romano }) => [decimal, romano])

// describe('Probar los números romanos del 0 al 1000', () => {
//   test.each<[number, string]>(romanos)("%s es %s", (decimal, romano) => {
//     expect(obtenerNumeroRomano(decimal)).toBe(romano)
//   })
// })
// ------------------------------------------------------------------
